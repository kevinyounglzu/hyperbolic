# -*- coding: utf8 -*-
from hyperbolic import fibtree


class NodeBase(object):
    def __init__(self, number, level):
        self.number = number
        self.level = level
        self.neighbors = []

    def addNeighbor(self, neighbor):
        self.neighbors.append(neighbor)

    def __repr__(self):
        return "<NodeBase %d at level %d>" % (self.number, self.level)


class HyperbolicGridBase(object):
    def __init__(self, level, Node_cls, whole_sectors):
        self.level = level - 2
        self.node_cls = Node_cls
        self.whole_sectors = whole_sectors
        self.lc = fibtree.LevelCounter()
        self.central_node = fibtree.CentralNode(self.whole_sectors)
        self.cordinates = []
        self.rcordinates = dict()
        self.generateMap()

        self.nodes = []
        self.generateNodes()

        self.tree = fibtree.FibTree(self.whole_sectors, self.level)
        self.connect()

    @property
    def length(self):
        return len(self.nodes)

    def getNode(self, index):
        return self.nodes[index]

    def generateNodes(self):
        """
        Generate all the nodes with self.node_cls
        """
        for i, node_cor in enumerate(self.cordinates):
            _, l = self.cordinates[i]
            self.nodes.append(self.node_cls(i, self.lc.findLevel(l) + 1))

    def connect(self):
        """
        Generate the connections
        """
        # central node
        central_node = self.nodes[0]
        central_neighbors = self.central_node.getNeighbors()
        for central_neighbor in central_neighbors:
            if central_neighbor in self.rcordinates:
                central_node.addNeighbor(self.rcordinates[central_neighbor])

        # other nodes
        for i in range(self.whole_sectors):
            #print "Sector %d" % i
            for tree_node in self.tree.getNodes():
                #print tree_node
                node = self.nodes[self.rcordinates[(tree_node.sector, tree_node.number)]]
                neighbors = tree_node.getNeighbors()[:self.whole_sectors]
                for neighbor in neighbors:
                    # needs more assertion
                    if neighbor in self.rcordinates:
                        neighbor_u_number = self.rcordinates[neighbor]
                        node.addNeighbor(neighbor_u_number)
            self.tree.sectorsAdd()

    def generateMap(self):
        """
        establish the relation between (s, n) to universal number        """
        central_cor = (-1, 0)
        self.cordinates.append(central_cor)
        self.rcordinates[central_cor] = 0

        counter = 0
        for l in range(self.level + 1):
            for s in range(self.whole_sectors):
                number_of_level = self.lc.numberOfNodeOfLevel(l)
                first = self.lc.firstOfLevel(l)
                for number in range(number_of_level):
                    counter += 1
                    self.cordinates.append((s, first + number))
                    self.rcordinates[(s, first + number)] = counter


class PentaGrid(HyperbolicGridBase):
    def __init__(self, level, Node_cls):
        super(PentaGrid, self).__init__(level, Node_cls, 5)


class HeptaGrid(HyperbolicGridBase):
    def __init__(self, level, Node_cls):
        super(HeptaGrid, self).__init__(level, Node_cls, 7)

if __name__ == "__main__":
    import sys
    l = int(sys.argv[1])
    #pg = PentaGrid(3, NodeBase)
    pg = HeptaGrid(l, NodeBase)
    for node in pg.nodes:
        print node, node.neighbors
    #print len(pg.nodes)
