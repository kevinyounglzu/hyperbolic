# -*- coding: utf8 -*-
from hyperbolic import fibonacci


class ZeckendorfCode(object):
    def __init__(self, input_):
        if isinstance(input_, int):
            self.fromInt(input_)
        elif isinstance(input_, str):
            self.fromStr(input_)
        else:
            raise TypeError("Input wrong type: %s" % str(type(input_)))

    @property
    def number(self):
        return self.__number

    @property
    def digits(self):
        return self.__digits

    def fromInt(self, number):
        self.__number = number
        digits_list = self.encodeToList(number)
        self.__digits = "".join(map(str, digits_list))

    def fromStr(self, digits):
        if not self.valiate(digits):
            raise DigtsNotValitedError("Digits not valite: %s" % digits)
        self.__digits = digits
        self.__number = self.decodeFromDigits(digits)

    def encodeToList(self, number):
        if number == 0:
            return [0]
        else:
            digits_list = []
            number_left = number
            fib_seq = fibonacci.fib_seq_bounded(number)[::-1][:-1]
            for fib in fib_seq:
                if fib <= number_left:
                    digits_list.append(1)
                    number_left -= fib
                else:
                    digits_list.append(0)
            assert not digits_list[0] == 0
            return digits_list

    def decodeFromDigits(self, digits):
        if not self.valiate(digits):
            raise DigtsNotValitedError("Digits not valite: %s" % digits)

        if len(digits) == 0:
            return 0
        else:
            fib_seq = fibonacci.fib_seq_n(len(digits) + 1)[::-1][:-1]
            return sum(map(lambda digit, fib: int(digit) * fib, digits, fib_seq))

    def valiate(self, digits):
        for digit in digits:
            if digit not in "10":
                return False
        return True


class DigtsNotValitedError(StandardError):
    pass
