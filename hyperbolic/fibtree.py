# -*- coding: utf8 -*-
from hyperbolic import fibonacci
from hyperbolic import ZeckendorfCode
from Queue import Queue


class LevelCounter(object):
    def __init__(self):
        self.fg = fibonacci.fib_gen()
        self.fib_seq = []
        self.last_seq = [1]

    def numberOfNodeOfLevel(self, level):
        return self.getNthFib(2 * level + 1)

    def firstOfLevel(self, level):
        return self.getNthFib(2 * level)

    def lastOfLevel(self, level):
        return self.getNthFib(2 * level + 2) - 1

    def sumOfNumberToLevel(self, level):
        return self.lastOfLevel(level)

    def getNthFib(self, n):
        if n < len(self.fib_seq):
            return self.fib_seq[n]
        else:
            for i in range(n + 1 - len(self.fib_seq)):
                self.fib_seq.append(self.fg.next())
            return self.fib_seq[n]

    def findLevel(self, n):
        if n == 0:
            return -1
        elif self.last_seq[-1] == n:
            return len(self.last_seq) - 1
        elif self.last_seq[-1] < n:
            while True:
                length = len(self.last_seq)
                next_last_n = self.sumOfNumberToLevel(length)
                if next_last_n >= n:
                    self.last_seq.append(next_last_n)
                    break
            return len(self.last_seq) - 1
        else:
            for i, number in enumerate(self.last_seq):
                if number >= n:
                    return i


class FibTreeNodeBase(object):
    def __init__(self, whole_sectors, sector, number):
        self.whole_sectors = whole_sectors
        self.sector = sector
        self.number = number
        self.zcode = ZeckendorfCode.ZeckendorfCode(self.number)
        self.digits = self.zcode.digits

    def findFather(self):
        """
        the father
        apply to all nodes
        """
        if len(self.digits) <= 1:
            return (self.sector, 0)
        else:
            m_digits = self.digits[:-2]
            self.zcode.fromStr(m_digits)
            m = self.zcode.number
            father = m + int(self.digits[-2])
            return (self.sector, father)

    def preferedSon(self):
        """
        [n]00 son
        apply to all nodes
        """
        if self.digits == "0":
            return (self.sector, 1)
        else:
            son_digits = self.digits + "00"
            self.zcode.fromStr(son_digits)
            son_number = self.zcode.number
            return (self.sector, son_number)

    def lastSon(self):
        """
        last son
        apply to all nodes
        """
        _, number = self.preferedSon()
        return (self.sector, number+1)

    def nextSSon(self):
        """
        next node's son
        apply to black nodes and whitew nodes
        """
        if self.number < 2:
            raise NodeError("Wrong methods on Node %d" % self.number)
        else:
            son_digits = self.digits + "10"
            self.zcode.fromStr(son_digits)
            son_number = self.zcode.number
            return (self.sector, son_number)

    def preNode(self):
        """
        previous node on the same level
        apply to all except left branches
        """
        return (self.sector, self.number-1)

    def nextNode(self):
        """
        next node on the same level
        apply to all except right branches
        """
        return (self.sector, self.number+1)

    def sectorAdd(self):
        self.sector = self.nextSector()
        return self.sector

    def nextSector(self):
        return (self.sector + 1) % self.whole_sectors

    def preSector(self):
        return (self.sector - 1) % self.whole_sectors

    def sectorSubstract(self):
        self.sector = self.preSector()
        return self.sector


class BlackNodeBase(FibTreeNodeBase):
    def preSSon(self):
        """
        previous node's son
        apply only to black nodes
        """
        _, number = self.findFather()
        return (self.sector, number-1)

    def getNeighbors(self):
        return [self.findFather(),
                self.preSSon(),
                self.preferedSon(),
                self.lastSon(),
                self.nextSSon(),
                self.preNode(),
                self.nextNode()
                ]


class BlackNode(BlackNodeBase):
    def __repr__(self):
        return "<BlackNode (%d, %d)@%d>" % (self.sector, self.number, self.whole_sectors)


class LeftBranchNode(BlackNodeBase):
    def preSSon(self):
        """
        previous node's son
        apply only to left branches
        """
        if self.number < 2:
            raise NodeError("Wrong methods on Node %d" % self.number)
        else:
            return (self.preSector(), self.number - 1)

    def preNode(self):
        """
        previous node
        apply only to left branches
        """
        if self.number < 2:
            raise NodeError("Wrong methods on Node %d" % self.number)
        else:
            self.zcode.fromInt(self.number - 1)
            node_digits = self.zcode.digits + "01"
            self.zcode.fromStr(node_digits)
            return (self.preSector(), self.zcode.number)

    def __repr__(self):
        return "<LeftBranchNode (%d, %d)@%d>" % (self.sector, self.number, self.whole_sectors)


class WhiteNodeBase(FibTreeNodeBase):
    def firstSon(self):
        """
        first son
        apply to white nodes
        """
        self.zcode.fromInt(self.number - 1)
        digits = self.zcode.digits + "10"
        self.zcode.fromStr(digits)
        return (self.sector, self.zcode.number)

    def getNeighbors(self):
        return [self.findFather(),
                self.firstSon(),
                self.preferedSon(),
                self.lastSon(),
                self.nextSSon(),
                self.preNode(),
                self.nextNode()
                ]


class WhiteNodeW(WhiteNodeBase):
    def __repr__(self):
        return "<WhiteNodeW (%d, %d)@%d>" % (self.sector, self.number, self.whole_sectors)


class WhiteNodeB(WhiteNodeBase):
    """
    next node's son
    apply only to whiteb nodes
    """
    def nextSSon(self):
        if self.number < 6:
            raise NodeError("Wrong methods on Node %d" % self.number)
        else:
            self.zcode.fromInt(self.number + 1)
            son_digits = self.zcode.digits + "00"
            self.zcode.fromStr(son_digits)
            return (self.sector, self.zcode.number)

    def __repr__(self):
        return "<WhiteNodeB (%d, %d)@%d>" % (self.sector, self.number, self.whole_sectors)


class RightBranchNode(WhiteNodeBase):
    def nextSSon(self):
        """
        next node's son
        apply only to right branches nodes
        """
        return (self.nextSector(), self.number + 1)

    def nextNode(self):
        """
        next node on the same level
        apply only to right branches nodes
        """
        _, father_number = self.findFather()
        return (self.nextSector(), father_number + 1)

    def __repr__(self):
        return "<RightBranchNode (%d, %d)@%d>" % (self.sector, self.number, self.whole_sectors)


class Root(RightBranchNode):
    def __init__(self, whole_sectors, sector, number):
        if number != 1:
            raise ValueError("Root must have number 1, but have %d instead" % number)
        super(Root, self).__init__(whole_sectors, sector, number)

    def findFather(self):
        return (-1, 0)

    def preNode(self):
        return (self.preSector(), 1)

    def __repr__(self):
        return "<Root (%d, %d)@%d>" % (self.sector, self.number, self.whole_sectors)


class CentralNode(FibTreeNodeBase):
    def __init__(self, whole_sectors):
        super(CentralNode, self).__init__(whole_sectors, -1, 0)

    def getNeighbors(self):
        return [(i, 1) for i in range(self.whole_sectors)]

    def __repr__(self):
        return "<CentralNode (%d, %d)@%d>" % (self.sector, self.number, self.whole_sectors)


class FibTree(object):
    def __init__(self, whole_sectors, level):
        self.whole_sectors = whole_sectors
        self.level = level

        self.level_counter = LevelCounter()
        self.number_of_nodes = self.level_counter.sumOfNumberToLevel(self.level)

        self.nodes = dict()

        self.left_branches = []
        for i in range(1, self.level + 2):
            self.left_branches.append(self.level_counter.firstOfLevel(i))

        self.right_branches = []
        for i in range(self.level + 2):
            self.right_branches.append(self.level_counter.lastOfLevel(i))

        self.generateSector(0)

    def generateSector(self, n):
        nodes_to_add = Queue()

        nodes_to_add.put((Root, 1))

        while not nodes_to_add.empty():
            Node_cls, number = nodes_to_add.get()
            #print Node_cls, number

            if number <= self.number_of_nodes:
                new_node = Node_cls(self.whole_sectors, n, number)
                # to do: check exsitence
                assert (n, number) not in self.nodes, "New node already exsits!"
                self.nodes[(n, number)] = new_node

                if isinstance(new_node, BlackNodeBase):
                    # prefered son
                    _, prefered_son = new_node.preferedSon()
                    if prefered_son in self.left_branches:
                        nodes_to_add.put((LeftBranchNode, prefered_son))
                    else:
                        nodes_to_add.put((BlackNode, prefered_son))
                    # last son
                    _, last_son = new_node.lastSon()
                    if last_son in self.right_branches:
                        nodes_to_add.put((RightBranchNode, last_son))
                    else:
                        nodes_to_add.put((WhiteNodeB, last_son))
                elif isinstance(new_node, WhiteNodeBase):
                    # first son
                    _, first_son = new_node.firstSon()
                    if first_son in self.left_branches:
                        nodes_to_add.put((LeftBranchNode, first_son))
                    else:
                        nodes_to_add.put((BlackNode, first_son))
                    # prefered son
                    _, prefered_son = new_node.preferedSon()
                    nodes_to_add.put((WhiteNodeW, prefered_son))
                    # last son
                    _, last_son = new_node.lastSon()
                    if last_son in self.right_branches:
                        nodes_to_add.put((RightBranchNode, last_son))
                    else:
                        nodes_to_add.put((WhiteNodeB, last_son))
                else:
                    raise NodeError("Wrong node type %s" % str(type(new_node)))

    def getNodes(self):
        return self.nodes.values()

    def sectorsAdd(self):
        for node in self.nodes.values():
            node.sectorAdd()

    def sectorsSubtract(self):
        for node in self.nodes.values():
            node.sectorSubstract()


class NodeError(StandardError):
    pass
