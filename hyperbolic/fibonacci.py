# -*- coding: utf8 -*-
def fib_gen():
    """
    Generator for fibonacci sequence
    """
    state = [1, 1]
    while True:
        state.append(sum(state))
        yield state.pop(0)


def fib_seq_bounded(max_fib):
    """
    Generate a fibonacci sequence with the last one samller than max_fib
    @param max_fib: int: the upper bound of the fibonacci sequence
    @return : a list of wanted fibonacci sequences
    """
    seq = []
    for fib in fib_gen():
        if fib > max_fib:
            break
        seq.append(fib)
    return seq


def fib_seq_n(n):
    """
    Generate a fibonacci sequence with n elements
    @param n: int: length of the wanted sequence
    @return : the wanted sequence
    """
    seq = []
    for i, fib in enumerate(fib_gen()):
        seq.append(fib)
        if i == n-1:
            break
    return seq
