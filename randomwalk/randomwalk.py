# -*- coding: utf8 -*-
from hyperbolic import lattice
import random


class RandomWalker(object):
    def __init__(self, whole_level, steps):
        self.steps = steps
        self.whole_level = whole_level
        self.lc = lattice.HeptaGrid(self.whole_level, lattice.NodeBase)
        self.reset()

    def reset(self):
        self.current_node = self.lc.getNode(0)

    def oneTimeStep(self):
        next_one = random.choice(self.current_node.neighbors)
        self.current_node = self.lc.getNode(next_one)

    def simu(self, f):
        for i in range(self.steps):
            f.write("%d " % self.current_node.level)
            self.oneTimeStep()


if __name__ == "__main__":
    for n in range(3, 12):
        print n
        rw = RandomWalker(n, 50)
        with open("./data/%d.txt" % n, "w") as f:
            for i in range(1000):
                rw.reset()
                rw.simu(f)
                f.write("\n")
