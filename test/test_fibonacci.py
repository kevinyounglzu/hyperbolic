# -*- coding: utf8 -*-
import unittest
from hyperbolic import fibonacci


class TestFibGenerator(unittest.TestCase):
    def setUp(self):
        self.fib_list = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144]

    def test_fib_gen(self):
        fg = fibonacci.fib_gen()
        for fib in self.fib_list:
            self.assertEqual(fib, fg.next())

    def test_fib_seq_bouned(self):
        result = fibonacci.fib_seq_bounded(self.fib_list[-1])
        for a, b in zip(self.fib_list, result):
            self.assertEqual(a, b)

    def test_fib_seq_n(self):
        for i in range(len(self.fib_list)):
            gl = fibonacci.fib_seq_n(i+1)
            for j in range(len(gl)):
                self.assertEqual(gl[j], self.fib_list[j])

if __name__ == "__main__":
    unittest.main()
