# -*- coding: utf8 -*-
import unittest
from hyperbolic import ZeckendorfCode


class TestZeckendorfCode(unittest.TestCase):
    def setUp(self):
        self.number_to_digits = ["0", "1", "10", "100", "101", "1000", "1001",
                                 "1010", "10000", "10001", "10010", "10100",
                                 "10101", "100000", "100001", "100010", "100100",
                                 "100101", "101000", "101001", "101010"]

    def test_init_wrong_type(self):
        # test wrong input type
        self.assertRaises(TypeError, ZeckendorfCode.ZeckendorfCode, 2.)

    def test_init_from_int(self):
        for i in range(len(self.number_to_digits)):
            zc = ZeckendorfCode.ZeckendorfCode(i)
            self.assertEqual(zc.digits, self.number_to_digits[i])
            self.assertEqual(zc.number, i)

    def test_init_from_str(self):
        for i in range(len(self.number_to_digits)):
            zc = ZeckendorfCode.ZeckendorfCode(self.number_to_digits[i])
            self.assertEqual(zc.digits, self.number_to_digits[i])
            self.assertEqual(zc.number, i)

    def test_valiate(self):
        zc = ZeckendorfCode.ZeckendorfCode(10)
        self.assertTrue(zc.valiate("1001"))
        self.assertFalse(zc.valiate("123"))

    def test_encodeToList(self):
        zc = ZeckendorfCode.ZeckendorfCode(10)
        for i in range(len(self.number_to_digits)):
            digits_list = zc.encodeToList(i)
            for a, b in zip(digits_list, self.number_to_digits[i]):
                self.assertEqual(str(a), b)

    def test_decode_from_digits(self):
        zc = ZeckendorfCode.ZeckendorfCode(10)
        self.assertRaises(ZeckendorfCode.DigtsNotValitedError, zc.decodeFromDigits, "123")

        self.assertEqual(zc.decodeFromDigits(""), 0)
        for i in range(len(self.number_to_digits)):
            self.assertEqual(
                zc.decodeFromDigits(self.number_to_digits[i]),
                i
            )

if __name__ == "__main__":
    unittest.main()
