# -*- coding: utf8 -*-
import unittest
from hyperbolic import fibtree


class TestLevelCounter(unittest.TestCase):
    def setUp(self):
        self.fib_list = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144]
        self.lc = fibtree.LevelCounter()

    def test_get_nth_fib(self):
        self.assertEqual(len(self.lc.fib_seq), 0)

        self.assertEqual(self.lc.getNthFib(0), self.fib_list[0])
        self.assertEqual(len(self.lc.fib_seq), 1)

        self.assertEqual(self.lc.getNthFib(1), self.fib_list[1])
        self.assertEqual(len(self.lc.fib_seq), 2)

        self.assertEqual(self.lc.getNthFib(5), self.fib_list[5])
        self.assertEqual(len(self.lc.fib_seq), 6)

        self.assertEqual(self.lc.getNthFib(1), self.fib_list[1])
        self.assertEqual(len(self.lc.fib_seq), 6)

    def test_number_of_node_level(self):
        levels = [1, 3, 8, 21]
        for i in range(len(levels)):
            self.assertEqual(levels[i], self.lc.numberOfNodeOfLevel(i))

    def test_first_of_level(self):
        fls = [1, 2, 5, 13]
        for i in range(len(fls)):
            self.assertEqual(fls[i], self.lc.firstOfLevel(i))

    def test_last_of_level(self):
        las = [1, 4, 12, 33]
        for i in range(len(las)):
            self.assertEqual(las[i], self.lc.lastOfLevel(i))

    def test_sum_of_level(self):
        for i in range(4):
            s = sum(self.lc.numberOfNodeOfLevel(j) for j in range(i+1))

            self.assertEqual(s, self.lc.sumOfNumberToLevel(i))

    def test_find_level(self):
        self.assertEqual(self.lc.last_seq, [1])

        self.assertEqual(self.lc.findLevel(1), 0)

        self.assertEqual(self.lc.last_seq, [1])

        level_1 = [2, 3, 4]
        for l in level_1:
            self.assertEqual(self.lc.findLevel(l), 1)
            self.assertEqual(self.lc.last_seq, [1, 4])

        level_2 = [5, 6, 7, 8, 9, 10, 11, 12]
        for l in level_2:
            self.assertEqual(self.lc.findLevel(l), 2)
            self.assertEqual(self.lc.last_seq, [1, 4, 12])

        level_3 = [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27,
                   28, 29, 30, 31, 32, 33]
        for l in level_3:
            self.assertEqual(self.lc.findLevel(l), 3)
            self.assertEqual(self.lc.last_seq, [1, 4, 12, 33])


class TestNodeBase(unittest.TestCase):
    def test_init(self):
        whole_sectors = 5
        sector = 1
        number = 10
        fb = fibtree.FibTreeNodeBase(whole_sectors, sector, number)
        self.assertEqual(fb.sector, sector)
        self.assertEqual(fb.number, number)
        self.assertEqual(fb.digits, "10010")

    def test_find_father(self):
        whole_sectors = 5
        sector = 1
        node_father = [0, 0, 1, 1, 1, 2, 2, 3, 3, 3, 4, 4, 4,
                       5, 5, 6, 6, 6, 7, 7, 8, 8, 8, 9, 9, 9,
                       10, 10, 11, 11, 11, 12, 12, 12]
        for i in range(len(node_father)):
            f_sector, f_number = fibtree.FibTreeNodeBase(whole_sectors, sector, i).findFather()
            self.assertEqual(f_number, node_father[i])
            self.assertEqual(f_sector, sector)

    def test_prefered_son(self):
        whole_sectors = 5
        sector = 1
        node_prefered_son = [1, 3, 5, 8, 11, 13, 16, 18, 21, 24, 26]
        for i in range(len(node_prefered_son)):
            s_sector, s_number = fibtree.FibTreeNodeBase(whole_sectors, sector, i).preferedSon()
            self.assertEqual(s_sector, sector)
            self.assertEqual(s_number, node_prefered_son[i])

    def test_last_son(self):
        whole_sectors = 5
        sector = 1
        node_last_son = [0, 4, 6, 9, 12, 14, 17, 19, 22, 25, 27]
        for i in range(1, len(node_last_son)):
            s_sector, s_number = fibtree.FibTreeNodeBase(whole_sectors, sector, i).lastSon()
            self.assertEqual(s_sector, sector)
            self.assertEqual(s_number, node_last_son[i])

    def test_next_s_son(self):
        whole_sectors = 5
        sector = 1
        self.assertRaises(fibtree.NodeError, fibtree.FibTreeNodeBase(whole_sectors, sector, 0).nextSSon)
        nodes = [2, 3, 5, 7, 8, 10, 11]
        next_sons = [7, 10, 15, 20, 23, 28, 31]
        for i in range(1, len(next_sons)):
            s_sector, s_number = fibtree.FibTreeNodeBase(whole_sectors, sector, nodes[i]).nextSSon()
            self.assertEqual(s_sector, sector)
            self.assertEqual(s_number, next_sons[i])

    def test_pre_node(self):
        whole_sectors = 5
        sector = 1
        nodes = [3, 4, 6, 7, 8, 9, 10, 11, 12]
        pre_nodes = [2, 3, 5, 6, 7, 8, 9, 10, 11]
        for i in range(len(pre_nodes)):
            p_sector, p_number = fibtree.FibTreeNodeBase(whole_sectors, sector, nodes[i]).preNode()
            self.assertEqual(p_sector, sector)
            self.assertEqual(p_number, pre_nodes[i])

    def test_next_node(self):
        whole_sectors = 5
        sector = 1
        nodes = [2, 3, 5, 6, 7, 8, 9, 10, 11]
        next_nodes = [3, 4, 6, 7, 8, 9, 10, 11, 12]
        for i in range(len(next_nodes)):
            n_sector, n_number = fibtree.FibTreeNodeBase(whole_sectors, sector, nodes[i]).nextNode()
            self.assertEqual(n_sector, sector)
            self.assertEqual(n_number, next_nodes[i])

    def test_sector_add_sub(self):
        whole_sectors = 5
        sectors = [0, 1, 2, 3, 4]
        sectors_add = [1, 2, 3, 4, 0]
        sectors_sub = [4, 0, 1, 2, 3]
        for s, sa in zip(sectors, sectors_add):
            self.assertEqual(fibtree.FibTreeNodeBase(whole_sectors, s, 10).nextSector(), sa)

        for s, ss in zip(sectors, sectors_sub):
            self.assertEqual(fibtree.FibTreeNodeBase(whole_sectors, s, 10).preSector(), ss)

    def test_sector_change(self):
        whole_sectors = 5
        sectors = [0, 1, 2, 3, 4]
        sectors_add = [1, 2, 3, 4, 0]
        sectors_sub = [4, 0, 1, 2, 3]
        for s, sa in zip(sectors, sectors_add):
            node = fibtree.FibTreeNodeBase(whole_sectors, s, 10)
            node.sectorAdd()
            self.assertEqual(node.sector, sa)

        for s, ss in zip(sectors, sectors_sub):
            node = fibtree.FibTreeNodeBase(whole_sectors, s, 10)
            node.sectorSubstract()
            self.assertEqual(node.sector, ss)


class TestWhiteNodeBase(unittest.TestCase):
    def test_first_son(self):
        whole_sectors = 5
        sector = 1
        nodes = [1, 3, 4, 6, 8, 9, 11, 12]
        first_sons = [2, 7, 10, 15, 20, 23, 28, 31]
        for node, first_son in zip(nodes, first_sons):
            s_sector, s_number = fibtree.WhiteNodeBase(whole_sectors, sector, node).firstSon()
            self.assertEqual(s_sector, sector)
            self.assertEqual(s_number, first_son)


class TestWhiteNodeB(unittest.TestCase):
    def test_next_s_son(self):
        whole_sectors = 5
        sector = 1
        nodes = [6, 9]
        next_sons = [18, 26]
        for node, next_son in zip(nodes, next_sons):
            s_sector, s_number = fibtree.WhiteNodeB(whole_sectors, sector, node).nextSSon()
            self.assertEqual(s_sector, sector)
            self.assertEqual(s_number, next_son)


class TestBlackNodeBase(unittest.TestCase):
    def test_pre_s_son(self):
        whole_sectors = 5
        sector = 1
        nodes = [7, 10, 15, 20, 23, 26, 28, 31]
        pre_s_sons = [2, 3, 5, 7, 8, 9, 10, 11]
        for node, pre_s_son in zip(nodes, pre_s_sons):
            s_sector, s_number = fibtree.BlackNodeBase(whole_sectors, sector, node).preSSon()
            self.assertEqual(s_sector, sector)
            self.assertEqual(s_number, pre_s_son)


class TestLeftBranch(unittest.TestCase):
    def test_pre_s_son(self):
        whole_sectors = 5

        self.assertRaises(fibtree.NodeError, fibtree.LeftBranchNode(whole_sectors, 1, 0).preSSon)
        self.assertRaises(fibtree.NodeError, fibtree.LeftBranchNode(whole_sectors, 1, 1).preSSon)

        sectors = [0, 1, 2, 3, 4]
        r_sectors = [4, 0, 1, 2, 3]
        nodes = [2, 5, 13]
        pre_sons = [1, 4, 12]
        for i in range(whole_sectors):
            for j in range(len(nodes)):
                s_sector, s_number = fibtree.LeftBranchNode(whole_sectors, sectors[i], nodes[j]).preSSon()
                self.assertEqual(s_sector, r_sectors[i])
                self.assertEqual(s_number, pre_sons[j])

    def test_pre_node(self):
        whole_sectors = 7
        sectors = range(7)
        r_sectors = [6, 0, 1, 2, 3, 4, 5]
        nodes = [2, 5, 13]
        pre_nodes = [4, 12, 33]
        for i in range(7):
            for j in range(len(pre_nodes)):
                p_sector, p_number = fibtree.LeftBranchNode(whole_sectors, sectors[i], nodes[j]).preNode()
                self.assertEqual(p_sector, r_sectors[i])
                self.assertEqual(p_number, pre_nodes[j])


class TestRightBranch(unittest.TestCase):
    def test_pre_s_son(self):
        whole_sectors = 5

        sectors = [0, 1, 2, 3, 4]
        r_sectors = [1, 2, 3, 4, 0]
        nodes = [1, 4, 12]
        next_sons = [2, 5, 13]
        for i in range(whole_sectors):
            for j in range(len(nodes)):
                s_sector, s_number = fibtree.RightBranchNode(whole_sectors, sectors[i], nodes[j]).nextSSon()
                self.assertEqual(s_sector, r_sectors[i])
                self.assertEqual(s_number, next_sons[j])

    def test_next_node(self):
        whole_sectors = 7
        sectors = range(7)
        r_sectors = [1, 2, 3, 4, 5, 6, 0]
        nodes = [4, 12, 33]
        next_nodes = [2, 5, 13]
        for i in range(whole_sectors):
            for j in range(len(nodes)):
                n_sector, n_number = fibtree.RightBranchNode(whole_sectors, sectors[i], nodes[j]).nextNode()
                self.assertEqual(n_sector, r_sectors[i])
                self.assertEqual(n_number, next_nodes[j])


class TestRoot(unittest.TestCase):
    def setUp(self):
        self.whole_sectors = 5
        self.sector = 1
        self.root = fibtree.Root(self.whole_sectors, self.sector, 1)

    def test_init(self):
        self.assertRaises(ValueError, fibtree.Root, self.whole_sectors, self.sector, 2)

    def test_father(self):
        s, n = self.root.findFather()
        self.assertEqual(s, -1)
        self.assertEqual(n, 0)

    def test_pre_s_son(self):
        s, n = self.root.firstSon()
        self.assertEqual(s, self.sector)
        self.assertEqual(n, 2)

    def test_prefered_son(self):
        s, n = self.root.preferedSon()
        self.assertEqual(s, self.sector)
        self.assertEqual(n, 3)

    def test_last_son(self):
        s, n = self.root.lastSon()
        self.assertEqual(s, self.sector)
        self.assertEqual(n, 4)

    def test_next_s_son(self):
        s, n = self.root.nextSSon()
        self.assertEqual(s, self.sector + 1)
        self.assertEqual(n, 2)

    def test_next_node(self):
        whole_sectors = 7
        sector = range(7)
        r_sectors = [1, 2, 3, 4, 5, 6, 0]
        for i in range(7):
            r_sector, r_number = fibtree.Root(whole_sectors, sector[i], 1).nextNode()
            self.assertEqual(r_number, 1)
            self.assertEqual(r_sector, r_sectors[i])

    def test_pre_node(self):
        whole_sectors = 7
        sector = range(7)
        r_sectors = [6, 0, 1, 2, 3, 4, 5]
        for i in range(7):
            r_sector, r_number = fibtree.Root(whole_sectors, sector[i], 1).preNode()
            self.assertEqual(r_number, 1)
            self.assertEqual(r_sector, r_sectors[i])


class TestCentralNode(unittest.TestCase):
    def test_init(self):
        cn = fibtree.CentralNode(5)
        self.assertEqual(cn.whole_sectors, 5)
        self.assertEqual(cn.sector, -1)
        self.assertEqual(cn.number, 0)

    def test_get_neighbors(self):
        whole_sectors = 7
        cn = fibtree.CentralNode(whole_sectors)
        neighbors = cn.getNeighbors()
        for i in range(whole_sectors):
            self.assertEqual((i, 1), neighbors[i])

    #def test_all(self):
        #whole_sectors = 5
        #sector = 1
        #print fibtree.Root(whole_sectors, sector, 1).getNeighbors()
        #print fibtree.LeftBranchNode(whole_sectors, sector, 13).getNeighbors()
        #print fibtree.RightBranchNode(whole_sectors, sector, 33).getNeighbors()
        #print fibtree.BlackNode(whole_sectors, sector, 10).getNeighbors()
        #print fibtree.WhiteNodeW(whole_sectors, sector, 11).getNeighbors()
        #print fibtree.WhiteNodeB(whole_sectors, sector, 9).getNeighbors()


class TestFibTree(unittest.TestCase):
    def test_init(self):
        ft = fibtree.FibTree(5, 3)
        self.assertEqual(ft.level, 3)
        self.assertEqual(ft.whole_sectors, 5)
        self.assertEqual(ft.number_of_nodes, 33)

        ft_new = fibtree.FibTree(5, 2)
        left_branches = [2, 5, 13]
        right_branches = [1, 4, 12, 33]
        self.assertEqual(ft_new.left_branches, left_branches)
        self.assertEqual(ft_new.right_branches, right_branches)

    def test_gen(self):
        # think how to test
        ft = fibtree.FibTree(5, 2)
        ft.generateSector(1)

    def test_sectors_add(self):
        ft = fibtree.FibTree(5, 3)
        for i in range(5):
            for node in ft.getNodes():
                self.assertEqual(node.sector, i)
            ft.sectorsAdd()
        for node in ft.getNodes():
            self.assertEqual(node.sector, 0)

    def test_sectors_subtract(self):
        ft = fibtree.FibTree(5, 3)
        sectors = [0, 4, 3, 2, 1]
        for i in sectors:
            for node in ft.getNodes():
                self.assertEqual(node.sector, i)
            ft.sectorsSubtract()
        for node in ft.getNodes():
            self.assertEqual(node.sector, 0)

if __name__ == "__main__":
    unittest.main()
